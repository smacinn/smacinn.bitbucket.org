// Demo js

// JQuery plugin for pushing a new password from the Web service into an element
(function ( $ ) {
 
    $.fn.generatePassword = function( options ) {
        var password = "";
        // set the default options.
        var settings = $.extend({
            url: "http://38.86.192.10/irestservice",
            format: "json",
            password: "generate",
            length: 6,
            numeric: false,
            nonAlphaNumeric: false,
            input: null
        }, options );
        
        var api = settings.url + "/" + settings.format;
        for(key in settings){
           if(key != "url" && key != "format"){
            if (settings[key] != null){
                api = api + "/" + key + "/" +settings[key];
            }
           }
        }
        // get the new password
        
        $.ajax({async :false,crossDomain:true,dataType:"json",url:api,success:
               function (data){
                password = data;
                }
        });
        // write the password into the selected element(s).
        return this.each(function() {
           $( this ).html(password);
        
        });
    };
}( jQuery ));